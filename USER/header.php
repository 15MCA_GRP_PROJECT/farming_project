<?php
ob_start();
session_start();
if(!isset($_SESSION['uid']))
{
	header("location:../index.php");
}
?>
<!--
author: W3layouts
author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html lang="en">
<head>
<title>FARMING ASSISTANT</title>
<!-- for-mobile-apps -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Floriculture Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
		function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- //for-mobile-apps -->
<link href="../css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<link href="../css/style.css" rel="stylesheet" type="text/css" media="all" />
<!-- js -->
<script type="text/javascript" src="../js/jquery-2.1.4.min.js"></script>
<!-- //js -->
<!-- pop-up-box -->
<link href="../css/popuo-box.css" rel="stylesheet" type="text/css" media="all" />
<!-- //pop-up-box -->
<!-- font-awesome icons -->
<link href="../css/font-awesome.css" rel="stylesheet"> 
<!-- //font-awesome icons -->
<link href="//fonts.googleapis.com/css?family=Work+Sans:100,200,300,400,500,600,700,800,900&amp;subset=latin-ext" rel="stylesheet">
<link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
<script>
function settypeuser(a)
{
	document.getElementById("type").value=a;
	$("#typediv").hide("slow");
}
</script>
<style>
.w3l_head1{
	text-transform:uppercase;
}
</style>

</head>
	
<body>
<!-- header -->
	<div class="header">
	<div class="w3_agileits_nav">
	<div class="container">
				<div class="w3ls-nav">
					<nav class="navbar navbar-default">
							<div class="navbar-header">
								<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
									<span class="sr-only">Toggle navigation</span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
								</button>
							</div>
						<!-- Collect the nav links, forms, and other content for toggling -->
						<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
							<ul class="nav navbar-nav">
								<li><a class="active" href="index.php">Home</a></li>
								<li class="dropdown">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown">View<b class="caret"></b></a>
										<ul class="dropdown-menu agile_short_dropdown" style="width: 213px;">
											<li><a href="viewproduct.php">View Product</a></li>
											<li><a href="viewoffer.php" >View offer</a></li>
											<li><a href="payment.php" >View Confirm order</a></li>
										</ul>
								</li>
								<li><a  href="../logout.php">Logout</a></li>
							</ul>
							<div class="clearfix"> </div>							
						</div>	
					</nav>		
			</div>

			</div>
		</div>
		<div class="container">
			
			<div class="agile_header_grid">
				<div class="w3_agile_logo">
					<h1><a href="index.php">Farming Assistant</a></h1>
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>
	</div>
<!-- //header -->
<!-- pop-up-box -->
	<div id="small-dialog" class="mfp-hide w3ls_small_dialog wthree_pop">
		<h3 class="agileinfo_sign">Sign In</h3>	
		<div class="agileits_signin_form">
			<form action="" method="post">
				<input type="email" name="email" placeholder="Your Email" required="">
				<input type="password" name="password" placeholder="Password" required="">
				<div class="agile_remember">
					<div class="agile_remember_left">
						<div class="check">
							<label class="checkbox"><input type="checkbox" name="checkbox"><i> </i>remember me</label>
						</div>
					</div>
					<div class="agile_remember_right">
						<a href="#">Forgot Password?</a>
					</div>
					<div class="clearfix"> </div>
				</div>
				<input type="submit" value="SIGN IN" name="login">
				<p>Don't have an account <a href="#small-dialog1" class="play-icon popup-with-zoom-anim">Sign Up</a></p>
				<div class="w3agile_social_icons">
					<ul>
						<li class="wthree_follow">Follow us on :</li>
						<li><a href="#" class="w3_agile_facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
						<li><a href="#" class="agile_twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
						<li><a href="#" class="w3_agile_dribble"><i class="fa fa-dribbble" aria-hidden="true"></i></a></li>
					</ul>	
				</div>
			</form>
		</div>
	</div>
	<div id="small-dialog1" class="mfp-hide w3ls_small_dialog wthree_pop">
		<h3 class="agileinfo_sign">Sign Up</h3>	
		<div class="agileits_signin_form">
		<div id="typediv">
			<label>Choose type</label>
			<div style="margin-bottom:20px">
				<input type="button" name="signup" value="USER" style="width:100px;float:left;margin-left:20px" onclick="settypeuser(this.value)">
				<input type="button" name="signup" value="FARMER" style="width:100px;float:left;margin-left:20px" onclick="settypeuser(this.value)">
				<input type="button" name="signup" value="RETAILER" style="width:100px;float:left;margin-left:20px" onclick="settypeuser(this.value)">
			</div>
		</div>
		<div style="height:70px"></div>
			<form action="" method="post" >
				<input type="text" name="fname" placeholder="First Name" required="">
				<input type="text" name="lname" placeholder="Last Name" required="">
				<input type="email" name="email" placeholder="Your Email" required="">
				<input type="text" name="address" placeholder="Your address" required="">
				<input type="hidden" name="type" id="type" placeholder="Your address" required="">
				<input type="password" name="password" placeholder="Password" required="">
				<input type="password" name="cpassword" placeholder="Confirm Password" required="">
				<input type="submit" name="signup" value="SIGN UP">
				<p>Already a member <a href="#small-dialog" class="play-icon popup-with-zoom-anim">Sign In</a></p>
				<div class="w3agile_social_icons">
					<ul>
						<li class="wthree_follow">Follow us on :</li>
						<li><a href="#" class="w3_agile_facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
						<li><a href="#" class="agile_twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
						<li><a href="#" class="w3_agile_dribble"><i class="fa fa-dribbble" aria-hidden="true"></i></a></li>
					</ul>	
				</div>
			</form>
		</div>
	</div>
<!-- //pop-up-box -->	
<script src="../js/jquery.magnific-popup.js" type="text/javascript"></script>
<script>
	$(document).ready(function() {
	$('.popup-with-zoom-anim').magnificPopup({
		type: 'inline',
		fixedContentPos: false,
		fixedBgPos: true,
		overflowY: 'auto',
		closeBtnInside: true,
		preloader: false,
		midClick: true,
		removalDelay: 300,
		mainClass: 'my-mfp-zoom-in'
	});
																	
	});
</script>
<!-- banner -->	
	<div class="banner">
		<div class="container">
			<h3>Farming is not just a job, it’s a way of life </h3>
			
			<div class="wthree_banner_grids">
				<div class="col-md-3 wthree_banner_grid">
					<span class="glyphicon glyphicon-grain" aria-hidden="true"></span>
					<h4>Grains</h4>
				</div>
				<div class="col-md-3 wthree_banner_grid">
					<span class="glyphicon glyphicon-tree-deciduous" aria-hidden="true"></span>
					<h4>Vegetables</h4>
				</div>
				<div class="col-md-3 wthree_banner_grid">
					<span class="glyphicon glyphicon-apple" aria-hidden="true"></span>
					<h4>Fruits</h4>
				</div>
				<div class="col-md-3 wthree_banner_grid">
					<span class="glyphicon glyphicon-asterisk" aria-hidden="true"></span>
					<h4>Poultry</h4>
				</div>
				<div class="clearfix"> </div>
			</div>
			<div class="agileits_more">
				<ul>
					
					<li><a href="#" class="hvr-icon-hang" data-toggle="modal" data-target="#myModal">Read More</a></li>
				</ul>
			</div>
		</div>
	</div>
<!-- //banner -->
<!-- bootstrap-pop-up -->
	<div class="modal video-modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModal">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					Floriculture
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>						
				</div>
				<section>
					<div class="modal-body">
						<img src="../images/g9.jpg" alt=" " class="img-responsive" />
						<p>Agriculture is the cultivation of land and breeding of animals and plants
						 to provide food, fiber, medicinal plants and other products to sustain and 
						 enhance life.[1] Agriculture was the key development in the rise of sedentary 
						 human civilization, whereby farming of domesticated species created food surpluses 
						 that enabled people to live in cities. The study of agriculture is known as agricultural 
						 science. The history of agriculture dates back thousands of years; people gathered wild grains 
						 at least 105,000 years ago, and began to plant them around 11,500 years ago, before they became 
						 domesticated. Pigs, sheep, and cattle were domesticated over 10,000 years ago. Crops originate from 
						 at least 11 regions of the world. Industrial agriculture based on large-scale monoculture has in the 
						 past century become the dominant agricultural method..
							<i>"AGRICULTURE is the most healthful,most useful and most noble employment of man.</i></p>
					</div>
				</section>
			</div>
		</div>
	</div>
