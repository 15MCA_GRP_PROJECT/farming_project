/*
SQLyog Community Edition- MySQL GUI v8.03 
MySQL - 5.0.51b-community-nt : Database - db_farmingassistant
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

CREATE DATABASE /*!32312 IF NOT EXISTS*/`db_farmingassistant` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `db_farmingassistant`;

/*Table structure for table `tb_answers` */

DROP TABLE IF EXISTS `tb_answers`;

CREATE TABLE `tb_answers` (
  `ansid` int(11) NOT NULL auto_increment,
  `answer` longtext,
  `quid` varchar(50) default NULL,
  `advname` varchar(50) default NULL,
  PRIMARY KEY  (`ansid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tb_answers` */

/*Table structure for table `tb_auctionrequest` */

DROP TABLE IF EXISTS `tb_auctionrequest`;

CREATE TABLE `tb_auctionrequest` (
  `ar_id` int(9) NOT NULL auto_increment,
  `product` varchar(30) default NULL,
  `u_id` varchar(50) default NULL,
  `amount` varchar(12) default NULL,
  `status` varchar(15) default '0',
  `date` varchar(12) default '0',
  `start_time` varchar(12) default '0',
  `end_time` varchar(20) default '0',
  `buyer` varchar(50) default '0',
  `auctionamount` int(20) default '0',
  `aucstatus` varchar(50) default '0',
  PRIMARY KEY  (`ar_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `tb_auctionrequest` */

insert  into `tb_auctionrequest`(`ar_id`,`product`,`u_id`,`amount`,`status`,`date`,`start_time`,`end_time`,`buyer`,`auctionamount`,`aucstatus`) values (1,'1','farha123@gmail.com','200','approved','2018-07-14','11:04:00','11:08:00','0',0,'0');

/*Table structure for table `tb_bidding` */

DROP TABLE IF EXISTS `tb_bidding`;

CREATE TABLE `tb_bidding` (
  `id` int(10) NOT NULL auto_increment,
  `username` varchar(100) default NULL,
  `productname` varchar(100) default NULL,
  `date` varchar(50) default NULL,
  `amount` int(100) default NULL,
  `sts` varchar(20) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `tb_bidding` */

insert  into `tb_bidding`(`id`,`username`,`productname`,`date`,`amount`,`sts`) values (1,'admin','POTATO','2018-07-14',40,NULL),(2,'reshma','POTATO','2018-07-14 05:35:20',60,'in');

/*Table structure for table `tb_cart` */

DROP TABLE IF EXISTS `tb_cart`;

CREATE TABLE `tb_cart` (
  `cartid` int(11) NOT NULL auto_increment,
  `pid` int(11) default NULL,
  `qunt` int(11) default NULL,
  `amt` int(11) default NULL,
  `tot` int(11) default NULL,
  `gstper` int(50) default NULL,
  `uid` varchar(50) default NULL,
  `date` date default NULL,
  `status` varchar(50) default 'Enter',
  `billno` varchar(50) default '0',
  PRIMARY KEY  (`cartid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tb_cart` */

/*Table structure for table `tb_category` */

DROP TABLE IF EXISTS `tb_category`;

CREATE TABLE `tb_category` (
  `cat_id` int(11) NOT NULL auto_increment,
  `category` varchar(50) default NULL,
  PRIMARY KEY  (`cat_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `tb_category` */

insert  into `tb_category`(`cat_id`,`category`) values (1,'vegitable');

/*Table structure for table `tb_chat` */

DROP TABLE IF EXISTS `tb_chat`;

CREATE TABLE `tb_chat` (
  `id` int(10) NOT NULL auto_increment,
  `username` varchar(30) default NULL,
  `friendname` varchar(30) default NULL,
  `chatdate` varchar(200) default NULL,
  `msg` longtext,
  `image` varchar(300) default '0',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `tb_chat` */

insert  into `tb_chat`(`id`,`username`,`friendname`,`chatdate`,`msg`,`image`) values (1,'farha123@gmail.com','roshan123@gmail.com','2018-07-14 10:50:50','hi\n','0'),(2,'roshan123@gmail.com','farha123@gmail.com','2018-07-14 10:51:40','HI\n','0');

/*Table structure for table `tb_login` */

DROP TABLE IF EXISTS `tb_login`;

CREATE TABLE `tb_login` (
  `logname` varchar(50) NOT NULL,
  `logpsw` varchar(50) default NULL,
  `logtype` varchar(50) default NULL,
  `status` varchar(50) default 'register',
  PRIMARY KEY  (`logname`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tb_login` */

insert  into `tb_login`(`logname`,`logpsw`,`logtype`,`status`) values ('admin@gmail.com','admin','ADMIN','approve'),('ram@gmail.com','123','USER','approve'),('raman123@gmail.com','123','FARMER','approve'),('reshma123@gmail.com','123','RETAILER','approve'),('roshan123@gmail.com','123','FARMER','approve');

/*Table structure for table `tb_productoffer` */

DROP TABLE IF EXISTS `tb_productoffer`;

CREATE TABLE `tb_productoffer` (
  `offerid` int(11) NOT NULL auto_increment,
  `pid` int(11) default NULL,
  `offer` varchar(50) default NULL,
  `fdate` date default NULL,
  `tdate` date default NULL,
  PRIMARY KEY  (`offerid`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `tb_productoffer` */

insert  into `tb_productoffer`(`offerid`,`pid`,`offer`,`fdate`,`tdate`) values (1,1,'10%','2018-07-14','2018-07-15');

/*Table structure for table `tb_products` */

DROP TABLE IF EXISTS `tb_products`;

CREATE TABLE `tb_products` (
  `id` int(9) NOT NULL auto_increment,
  `name` varchar(50) default NULL,
  `disc` varchar(50) default NULL,
  `price` varchar(50) default NULL,
  `dstatus` varchar(50) default NULL,
  `path` varchar(50) default NULL,
  `auctionqty` int(11) default NULL,
  `salesqty` int(11) default NULL,
  `uid` varchar(50) default NULL,
  `category` int(11) default NULL,
  `subcat` int(11) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `tb_products` */

insert  into `tb_products`(`id`,`name`,`disc`,`price`,`dstatus`,`path`,`auctionqty`,`salesqty`,`uid`,`category`,`subcat`) values (1,'POTATO','Its good for heath\r\n','30','A','products/1.jpg',10,3,'farha123@gmail.com',1,1);

/*Table structure for table `tb_question` */

DROP TABLE IF EXISTS `tb_question`;

CREATE TABLE `tb_question` (
  `qid` int(11) NOT NULL auto_increment,
  `question` longtext,
  `username` varchar(20) default NULL,
  PRIMARY KEY  (`qid`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `tb_question` */

insert  into `tb_question`(`qid`,`question`,`username`) values (1,'which fertilizer is used in winter season?','roshan123@gmail.com');

/*Table structure for table `tb_subcategory` */

DROP TABLE IF EXISTS `tb_subcategory`;

CREATE TABLE `tb_subcategory` (
  `subcatid` int(11) NOT NULL auto_increment,
  `catid` int(11) default NULL,
  `subcat` varchar(20) default NULL,
  PRIMARY KEY  (`subcatid`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `tb_subcategory` */

insert  into `tb_subcategory`(`subcatid`,`catid`,`subcat`) values (1,1,'potato');

/*Table structure for table `tb_userregistration` */

DROP TABLE IF EXISTS `tb_userregistration`;

CREATE TABLE `tb_userregistration` (
  `fname` varchar(50) default NULL,
  `lname` varchar(50) default NULL,
  `address` varchar(50) default NULL,
  `email` varchar(50) NOT NULL,
  `imgpath` varchar(500) default NULL,
  PRIMARY KEY  (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tb_userregistration` */

insert  into `tb_userregistration`(`fname`,`lname`,`address`,`email`,`imgpath`) values ('farha','faru','valiyakath house','farha123@gmail.com','idproof/farha123@gmail.com.jpg'),('ram','krishna','palakkal house','ram@gmail.com','idproof/ram@gmail.com.jpg'),('raman','chandran','valiyakath house','raman123@gmail.com','idproof/raman123@gmail.com.jpg'),('reshma','nasar','kundakam house','reshma123@gmail.com','idproof/reshma123@gmail.com.jpg'),('roshan','abdullah','valiyakath house','roshan123@gmail.com','idproof/roshan123@gmail.com.jpg');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
