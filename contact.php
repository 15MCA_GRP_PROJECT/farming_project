<?php
include("subhead.php");
?>
<!-- contact -->
<div class="contact">
		<div class="container">
				<h3 class="w3l_head w3l_head1">Contact</h3>
				<p class="w3ls_head_para w3ls_head_para1">Farming Assistant</p>
			<div class="w3_contact_grids" style="margin-left:40%">

			<p><i class="glyphicon glyphicon-user"></i> &nbsp;Anand</p>
			<p><i class="glyphicon glyphicon-map-marker"></i> &nbsp;pavaratty,Thrissur</p>
			<p><i class="glyphicon glyphicon-envelope"></i> &nbsp;anand123@gmail.com</p>
			<p><i class="glyphicon glyphicon-earphone"></i> &nbsp;8943234510</p>
			

			</div>
		</div>
	</div>
	<script src="js/classie.js"></script>
	<script>
			(function() {
				// trim polyfill : https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/Trim
				if (!String.prototype.trim) {
					(function() {
						// Make sure we trim BOM and NBSP
						var rtrim = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;
						String.prototype.trim = function() {
							return this.replace(rtrim, '');
						};
					})();
				}

				[].slice.call( document.querySelectorAll( 'input.input__field' ) ).forEach( function( inputEl ) {
					// in case the input is already filled..
					if( inputEl.value.trim() !== '' ) {
						classie.add( inputEl.parentNode, 'input--filled' );
					}

					// events:
					inputEl.addEventListener( 'focus', onInputFocus );
					inputEl.addEventListener( 'blur', onInputBlur );
				} );

				function onInputFocus( ev ) {
					classie.add( ev.target.parentNode, 'input--filled' );
				}

				function onInputBlur( ev ) {
					if( ev.target.value.trim() === '' ) {
						classie.remove( ev.target.parentNode, 'input--filled' );
					}
				}
			})();
		</script>

<!-- //contact -->
	<div class="agile-map">
		<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d805196.5085218168!2d144.49269841104956!3d-37.97015416997174!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6ad646b5d2ba4df7%3A0x4045675218ccd90!2sMelbourne+VIC%2C+Australia!5e0!3m2!1sen!2sin!4v1475231100323"  allowfullscreen></iframe>
	</div>
<?php
include("footer.php");
?>