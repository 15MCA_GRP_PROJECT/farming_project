<?php
include("subhead.php");
?>
<style>
#divcat{
	width:100%;
	height:500px;
}
#inddiv{
	background-color:white;
	color:black;
	border:2px black solid;
	border-radius:5px;
	margin-left:10px;
	width:200px;
	height:100px;
	float:left;
	margin-bottom:20px;
}
.h2{
	text-align: center;
    color: #212121;
    padding-bottom: .5em;
    position: relative;
    font-size: 2.5em;
    text-transform: uppercase;
}
.h2:after{
	content: '';
    background: #3399cc;
    height: 2px;
    width: 15%;
    position: absolute;
    bottom: 0%;
    left: 43%;
    box-sizing: border-box;
}
.overlay {
  position: relative;
  bottom: 0;
  left: 0;
  right: 0;
  top: -60px;
  background-color: #4FC242;
  overflow: hidden;
  width: 100%;
  height: 100%;
  -webkit-transform:scale(0);
  transition: .3s ease;
  border-radius:5px;
}

#inddiv:hover .overlay {
  transform: scale(1)
}

.text {
  color: white;
  font-size: 20px;
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  -ms-transform: translate(-50%, -50%);
  text-align: center;
}
 .image{
	font-size:large;
	text-align:center;
	margin-top:35px;
}
</style>
<h2 class="w3l_head w3l_head1" style="margin-top:15px" >VIEW CATEGORY</h2>
<div id="divcat">
	

<?php
	include("../connect.php");
	$sql="select * from tb_category";
	$sq=mysql_query($sql);
	while($s=mysql_fetch_array($sq))
	{
?>
	<div id="inddiv">
	<div class="image"><?php echo $s[1];?></div>
		<div class="overlay">
    	   	<div class="text">
    	   		<a href="editcat.php?id=<?php echo $s[0];?>"><img src="../images/edit.png"  style="color:white;margin-left:-12px"></a>
    	   		<a href="deletecat.php?id=<?php echo $s[0];?>"><img src="../images/delete.png" style="color:white;margin-left:2px"></a>
    	   	</div>
  		</div>	
  </div>
<?php
	}
?>
</div>

<?php
include("footer.php");
?>
