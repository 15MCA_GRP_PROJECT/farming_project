-- phpMyAdmin SQL Dump
-- version 2.11.6
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 20, 2018 at 07:31 AM
-- Server version: 5.0.51
-- PHP Version: 5.2.6

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db_farmingassistant`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_answers`
--

CREATE TABLE `tb_answers` (
  `ansid` int(11) NOT NULL auto_increment,
  `answer` longtext,
  `quid` varchar(50) default NULL,
  `advname` varchar(50) default NULL,
  PRIMARY KEY  (`ansid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `tb_answers`
--


-- --------------------------------------------------------

--
-- Table structure for table `tb_auctionrequest`
--

CREATE TABLE `tb_auctionrequest` (
  `ar_id` int(9) NOT NULL auto_increment,
  `product` varchar(30) default NULL,
  `u_id` varchar(50) default NULL,
  `amount` varchar(12) default NULL,
  `status` varchar(15) default '0',
  `date` varchar(12) default '0',
  `start_time` varchar(12) default '0',
  `end_time` varchar(20) default '0',
  `buyer` varchar(50) default '0',
  `auctionamount` int(20) default '0',
  `aucstatus` varchar(50) default '0',
  PRIMARY KEY  (`ar_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `tb_auctionrequest`
--

INSERT INTO `tb_auctionrequest` (`ar_id`, `product`, `u_id`, `amount`, `status`, `date`, `start_time`, `end_time`, `buyer`, `auctionamount`, `aucstatus`) VALUES
(1, '1', 'farha123@gmail.com', '200', 'approved', '2018-07-14', '11:04:00', '11:08:00', '0', 0, '0'),
(2, '2', 'favas123@gmail.com', '300', '0', '0', '0', '0', '0', 0, '0');

-- --------------------------------------------------------

--
-- Table structure for table `tb_bidding`
--

CREATE TABLE `tb_bidding` (
  `id` int(10) NOT NULL auto_increment,
  `username` varchar(100) default NULL,
  `productname` varchar(100) default NULL,
  `date` varchar(50) default NULL,
  `amount` int(100) default NULL,
  `sts` varchar(20) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `tb_bidding`
--

INSERT INTO `tb_bidding` (`id`, `username`, `productname`, `date`, `amount`, `sts`) VALUES
(1, 'admin', 'POTATO', '2018-07-14', 40, NULL),
(2, 'reshma', 'POTATO', '2018-07-14 05:35:20', 60, 'in');

-- --------------------------------------------------------

--
-- Table structure for table `tb_cart`
--

CREATE TABLE `tb_cart` (
  `cartid` int(11) NOT NULL auto_increment,
  `pid` int(11) default NULL,
  `qunt` int(11) default NULL,
  `amt` int(11) default NULL,
  `tot` int(11) default NULL,
  `gstper` int(50) default NULL,
  `uid` varchar(50) default NULL,
  `date` date default NULL,
  `status` varchar(50) default 'Enter',
  `billno` varchar(50) default '0',
  PRIMARY KEY  (`cartid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `tb_cart`
--


-- --------------------------------------------------------

--
-- Table structure for table `tb_category`
--

CREATE TABLE `tb_category` (
  `cat_id` int(11) NOT NULL auto_increment,
  `category` varchar(50) default NULL,
  PRIMARY KEY  (`cat_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `tb_category`
--

INSERT INTO `tb_category` (`cat_id`, `category`) VALUES
(1, 'vegitable'),
(2, 'fruits'),
(3, 'vegitable'),
(4, 'vegitable');

-- --------------------------------------------------------

--
-- Table structure for table `tb_chat`
--

CREATE TABLE `tb_chat` (
  `id` int(10) NOT NULL auto_increment,
  `username` varchar(30) default NULL,
  `friendname` varchar(30) default NULL,
  `chatdate` varchar(200) default NULL,
  `msg` longtext,
  `image` varchar(300) default '0',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `tb_chat`
--

INSERT INTO `tb_chat` (`id`, `username`, `friendname`, `chatdate`, `msg`, `image`) VALUES
(1, 'farha123@gmail.com', 'roshan123@gmail.com', '2018-07-14 10:50:50', 'hi\n', '0'),
(2, 'roshan123@gmail.com', 'farha123@gmail.com', '2018-07-14 10:51:40', 'HI\n', '0'),
(3, 'favas123@gmail.com', 'raman123@gmail.com', '2018-07-17 14:01:44', 'hi', '0'),
(4, 'favas123@gmail.com', 'raman123@gmail.com', '2018-07-17 14:01:46', '', '0');

-- --------------------------------------------------------

--
-- Table structure for table `tb_login`
--

CREATE TABLE `tb_login` (
  `logname` varchar(50) NOT NULL,
  `logpsw` varchar(50) default NULL,
  `logtype` varchar(50) default NULL,
  `status` varchar(50) default 'register',
  PRIMARY KEY  (`logname`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_login`
--

INSERT INTO `tb_login` (`logname`, `logpsw`, `logtype`, `status`) VALUES
('admin@gmail.com', 'admin', 'ADMIN', 'approve'),
('anjukp@gmail.com', '1234', '', 'approve'),
('ereshma95@gmail.com', 'reshma95', '', 'approve'),
('favas123@gmail.com', '123', 'FARMER', 'approve'),
('prathila123@gmail.com', 'asdfg123', 'USER', 'approve'),
('ram@gmail.com', '123', 'USER', 'approve'),
('raman123@gmail.com', '123', 'FARMER', 'approve'),
('reshma123@gmail.com', '123', 'RETAILER', 'approve'),
('reshmamurali@gmail.com', 'reshmam', '', 'approve'),
('roshan123@gmail.com', '123', 'FARMER', 'approve'),
('surega@gmail.com', 'surega', 'FARMER', 'register'),
('unnik123@gmail.com', '123', 'USER', 'approve');

-- --------------------------------------------------------

--
-- Table structure for table `tb_productoffer`
--

CREATE TABLE `tb_productoffer` (
  `offerid` int(11) NOT NULL auto_increment,
  `pid` int(11) default NULL,
  `offer` varchar(50) default NULL,
  `fdate` date default NULL,
  `tdate` date default NULL,
  PRIMARY KEY  (`offerid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tb_productoffer`
--

INSERT INTO `tb_productoffer` (`offerid`, `pid`, `offer`, `fdate`, `tdate`) VALUES
(1, 1, '10%', '2018-07-14', '2018-07-15');

-- --------------------------------------------------------

--
-- Table structure for table `tb_products`
--

CREATE TABLE `tb_products` (
  `id` int(9) NOT NULL auto_increment,
  `name` varchar(50) default NULL,
  `disc` varchar(50) default NULL,
  `price` varchar(50) default NULL,
  `dstatus` varchar(50) default NULL,
  `path` varchar(50) default NULL,
  `auctionqty` int(11) default NULL,
  `salesqty` int(11) default NULL,
  `uid` varchar(50) default NULL,
  `category` int(11) default NULL,
  `subcat` int(11) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `tb_products`
--

INSERT INTO `tb_products` (`id`, `name`, `disc`, `price`, `dstatus`, `path`, `auctionqty`, `salesqty`, `uid`, `category`, `subcat`) VALUES
(1, 'POTATO', 'Its good for heath\r\n', '30', 'A', 'products/1.jpg', 10, 3, 'farha123@gmail.com', 1, 1),
(2, 'BEETROOT', 'healthy', '40', 'A', 'products/2.jpg', 10, 3, 'favas123@gmail.com', 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `tb_question`
--

CREATE TABLE `tb_question` (
  `qid` int(11) NOT NULL auto_increment,
  `question` longtext,
  `username` varchar(20) default NULL,
  PRIMARY KEY  (`qid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tb_question`
--

INSERT INTO `tb_question` (`qid`, `question`, `username`) VALUES
(1, 'which fertilizer is used in winter season?', 'roshan123@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `tb_subcategory`
--

CREATE TABLE `tb_subcategory` (
  `subcatid` int(11) NOT NULL auto_increment,
  `catid` int(11) default NULL,
  `subcat` varchar(20) default NULL,
  PRIMARY KEY  (`subcatid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `tb_subcategory`
--

INSERT INTO `tb_subcategory` (`subcatid`, `catid`, `subcat`) VALUES
(1, 1, 'potato'),
(2, 1, 'beetroot'),
(3, 3, 'carrot');

-- --------------------------------------------------------

--
-- Table structure for table `tb_userregistration`
--

CREATE TABLE `tb_userregistration` (
  `fname` varchar(50) default NULL,
  `lname` varchar(50) default NULL,
  `address` varchar(50) default NULL,
  `email` varchar(50) NOT NULL,
  `imgpath` varchar(500) default NULL,
  PRIMARY KEY  (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_userregistration`
--

INSERT INTO `tb_userregistration` (`fname`, `lname`, `address`, `email`, `imgpath`) VALUES
('anju', 'kp', 'anjanam', 'anjukp@gmail.com', 'idproof/anjukp@gmail.com.jpg'),
('reshma', 'e', 'kannath', 'ereshma95@gmail.com', 'idproof/ereshma95@gmail.com.jpg'),
('farha', 'faru', 'valiyakath house', 'farha123@gmail.com', 'idproof/farha123@gmail.com.jpg'),
('favas', 'kv', 'palakkal house', 'favas123@gmail.com', 'idproof/favas123@gmail.com.jpg'),
('prathila', 'k p', 'anjanam', 'prathila123@gmail.com', 'idproof/prathila123@gmail.com.jpg'),
('ram', 'krishna', 'palakkal house', 'ram@gmail.com', 'idproof/ram@gmail.com.jpg'),
('raman', 'chandran', 'valiyakath house', 'raman123@gmail.com', 'idproof/raman123@gmail.com.jpg'),
('reshma', 'nasar', 'kundakam house', 'reshma123@gmail.com', 'idproof/reshma123@gmail.com.jpg'),
('reshma', 'murali', 'reshmam', 'reshmamurali@gmail.com', 'idproof/reshmamurali@gmail.com.jpg'),
('roshan', 'abdullah', 'valiyakath house', 'roshan123@gmail.com', 'idproof/roshan123@gmail.com.jpg'),
('suregha', ' p p', 'palakkal house', 'surega@gmail.com', 'idproof/surega@gmail.com.jpg'),
('unni', 'krishnan', 'valiyakath house', 'unnik123@gmail.com', 'idproof/unnik123@gmail.com.jpg');
