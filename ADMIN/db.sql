/*
SQLyog Community Edition- MySQL GUI v8.03 
MySQL - 5.0.51b-community-nt : Database - db_chatadv
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

CREATE DATABASE /*!32312 IF NOT EXISTS*/`db_chatadv` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `db_chatadv`;

/*Table structure for table `tb_answers` */

DROP TABLE IF EXISTS `tb_answers`;

CREATE TABLE `tb_answers` (
  `ansid` int(11) NOT NULL auto_increment,
  `answer` varchar(50) default NULL,
  `quid` varchar(50) default NULL,
  PRIMARY KEY  (`ansid`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

/*Data for the table `tb_answers` */

insert  into `tb_answers`(`ansid`,`answer`,`quid`) values (1,'vdfgfdg','1'),(2,'dsfgsdg','1'),(3,'dfsdfsd','1'),(4,'fsdf','1'),(5,'sadasdasd','1'),(6,'sdasd','1'),(7,'dfdsfsdf','1'),(8,'this is a example','2');

/*Table structure for table `tb_question` */

DROP TABLE IF EXISTS `tb_question`;

CREATE TABLE `tb_question` (
  `qid` int(11) NOT NULL auto_increment,
  `question` varchar(100) default NULL,
  PRIMARY KEY  (`qid`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

/*Data for the table `tb_question` */

insert  into `tb_question`(`qid`,`question`) values (1,'fgdfgdfg'),(2,'dsfaafaf'),(3,''),(4,''),(5,''),(6,''),(7,'dfdsfsdf'),(8,'dsfsdf'),(9,'sdfsdf');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
