<!--
author: W3layouts
author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<?php
ob_start();
session_start();
if(!isset($_SESSION['uid']))
{
	header("location:../index.php");
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
<title>FARMING ASSISTANT</title>
<!-- for-mobile-apps -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Floriculture Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
		function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- //for-mobile-apps -->
<link href="../css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<link href="../css/style.css" rel="stylesheet" type="text/css" media="all" />
<!-- js -->
<script type="text/javascript" src="../js/jquery-2.1.4.min.js"></script>
<!-- //js -->
<!-- pop-up-box -->
<link href="../css/popuo-box.css" rel="stylesheet" type="text/css" media="all" />
<!-- //pop-up-box -->
<!-- font-awesome icons -->
<link href="../css/font-awesome.css" rel="stylesheet"> 
<!-- //font-awesome icons -->
<link href="//fonts.googleapis.com/css?family=Work+Sans:100,200,300,400,500,600,700,800,900&amp;subset=latin-ext" rel="stylesheet">
<link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$(".scroll").click(function(event){		
			event.preventDefault();
			$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
		});
	});
	function carttotcalc(a)
	{
		var p=document.getElementById("amtc"+a).value;
		var q=document.getElementById("qtyc"+a).value;
		if(q=="")
		{
			q=0;
		}
		//alert(q);
		var c=document.getElementById("cidc"+a).value;
		var url1="calctot.php";
		$.post(url1,{p:p,q:q,c:c},function(data){
			var res=data.split(".");
			//alert(res[0]);
			var str1= res[0].replace(/^\s*/, "").replace(/\s*$/, "");
			var str2= res[1].replace(/^\s*/, "").replace(/\s*$/, "");
				//	alert("str1:"+str1+"  str2:"+str2);
		document.getElementById("gtotc1").value=str1;
		document.getElementById("gtotc").innerHTML=str1;
		document.getElementById("totc"+a).value=str2;
		});

	}

</script>

<style>
.w3l_head1{
	text-transform:uppercase;
}
</style>
</head>
	
<body>
<!-- header -->
	<div class="header">
	<div class="w3_agileits_nav">
	<div class="container">
				<div class="w3ls-nav">
					<nav class="navbar navbar-default">
							<div class="navbar-header">
								<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
									<span class="sr-only">Toggle navigation</span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
								</button>
							</div>
						<!-- Collect the nav links, forms, and other content for toggling -->
						<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
							<ul class="nav navbar-nav">
								<li><a class="active" href="index.php">Home</a></li>
								<li class="dropdown">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown">View<b class="caret"></b></a>
										<ul class="dropdown-menu agile_short_dropdown" style="width: 213px;">
											<li><a href="viewproduct.php">View Product</a></li>
											<li><a href="viewoffer.php" >View offer</a></li>
											<li><a href="payment.php" >View Confirm order</a></li>
										</ul>
								</li>
								<li><a  href="../logout.php">Logout</a></li>
							</ul>
							<div class="product_list_header" style="margin-left:1150px">  
									<form action="#" method="post" class="last"> 
										<input type="hidden" name="cmd" value="_cart">
										<input type="hidden" name="display" value="1">
										<button class="w3view-cart" style="background-color: #414641;border:  none;" type="button" name="submit" data-toggle="modal" data-target="#cartmodel"><i class="fa fa-cart-arrow-down" aria-hidden="true" style="background-color:#414641;color: #fff;font-size: 23px;margin-left: -35px;margin-top: -4px;"></i></button>
									</form>  
							</div>
							<!-- Modal -->
							<div id="cartmodel" class="modal fade" role="dialog">
							  <div class="modal-dialog">
							
							    <!-- Modal content-->
							    <div class="modal-content" style="width:650px;margin-left: 125px;">
							      <div class="modal-header">
							        <button type="button" class="close" data-dismiss="modal">&times;</button>
							        <h4 class="modal-title">Your cart</h4>
							      </div>
							      <div class="modal-body">
							    <?php
							    	include("../connect.php");
							    	$uid=$_SESSION['uid'];
							    	$cart="SELECT `tb_products`.`name`,`tb_products`.`path`,`tb_cart`.* FROM `tb_products` INNER JOIN `tb_cart` ON `tb_products`.`id`=`tb_cart`.`pid` WHERE `tb_cart`.`uid`='$uid' and status='Enter'";
							    	//echo $cart;
							    	$car=mysql_query($cart);
							    	$countcart=mysql_num_rows($car);
							    	if($countcart>0)
							    	{
							    	
							    ?>
							      <form method="post" action="">
								      <table class="table table-striped" style="width:100%;">
										<thead class="alert alert-warning">
											<tr>
												<th>Image</th>
												<th>Name</th>
												<th>Price</th>
												<th>Quantity</th>
												<th>Total Price</th>
											</tr>
										</thead>
										<tbody>
								        <?php
								        	$t=0;
									    	$a=1;
									    	while($ca=mysql_fetch_array($car))
									    	{
							    		 ?>
												<tr>
													<td><img src="../<?php echo $ca[1];?>" style="width:50px;height:50px"></td>
													<td><?php echo $ca[0];?><input type="hidden" name="cid<?php echo $a;?>" id="cidc<?php echo $a;?>" value="<?php echo $ca[2];?>"> </td>
													<td><?php echo $ca[5];?><input type="hidden"name="amt" id="amtc<?php echo $a;?>" value="<?php echo $ca[5];?>" > </td>
													<td><input type="text" name="qty<?php echo $a;?>" id="qtyc<?php echo $a;?>" style="width:50px" value="<?php echo $ca[4];?>" autocomplete="off" class="form-control" required onkeyup="carttotcalc(<?php echo $a;?>)"></td>
													<td><input type="text" name="total<?php echo $a;?>" id="totc<?php echo $a;?>" class="form-control" value="<?php echo $ca[6]; ?>" style="border:none;width:100px;color:#C90A30" ></td>
												</tr>
										<?php
										$t+=$ca[6];
										//echo "<script>alert('$ca[6]');</script>";
										$a++;
											}
										?>
										<tr>
											<td colspan="2"></td>
											<td colspan="2">Total:<span id="gtotc"><?php echo $t;?></span><input type="hidden" name="tot1" id="gtotc1" value="<?php echo $t;?>"></td>
											<td colspan="2"></td>
										</tr>
										<tr>
											<td colspan="6"><button type="submit" name="confirm" class="btn btn-success">CONFIRM</button></td>
										</tr>
								        </tbody>
								        </table>
							        </form>
							        <?php
							        }
							        else
							        {
							        	echo "No Cart Product";
							        }
							        ?>
							      </div>
							      <?php 
							      	if(isset($_POST['confirm']))
							      	{
							      		$count=$a;
							      		for($i=1;$i<$count;$i++)
							      		{
							      			$cid=$_POST['cid'.$i];
							      			$qty=$_POST['qty'.$i];
							      			$tot=$_POST['total'.$i];
											$update1="update tb_cart set `qunt`='$qty',`tot`='$tot',status='confirm' where cartid='$cid'";
											$upd1=mysql_query($update1);
							      		}
							      		if($upd1>0)
							      		{
							      			echo "<script>window.location.href='payment.php';</script>";
							      		}
							      		else
							      		{
							      			echo "<script>alert('Error');window.location.href='viewproduct.php';</script>";
							      		}
							       	}
							      ?>
							      <div class="modal-footer">
							        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
							      </div>
							    </div>							
							    <div class="clearfix"> </div>							
						</div>	
					</nav>		
			</div>

			</div>
		</div>
		<div class="container">
			
			<div class="agile_header_grid">
				<div class="w3_agile_logo">
					<h1><a href="index.php">Farming Assistant</a></h1>
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>
	</div>
<!-- //header -->
<!-- pop-up-box -->
	<div id="small-dialog" class="mfp-hide w3ls_small_dialog wthree_pop">
		<h3 class="agileinfo_sign">Sign In</h3>	
		<div class="agileits_signin_form">
			<form action="#" method="post">
				<input type="email" name="email" placeholder="Your Email" required="">
				<input type="password" name="password" placeholder="Password" required="">
				<div class="agile_remember">
					<div class="agile_remember_left">
						<div class="check">
							<label class="checkbox"><input type="checkbox" name="checkbox"><i> </i>remember me</label>
						</div>
					</div>
					<div class="agile_remember_right">
						<a href="#">Forgot Password?</a>
					</div>
					<div class="clearfix"> </div>
				</div>
				<input type="submit" value="SIGN IN">
				<p>Don't have an account <a href="#small-dialog1" class="play-icon popup-with-zoom-anim">Sign Up</a></p>
				<div class="w3agile_social_icons">
					<ul>
						<li class="wthree_follow">Follow us on :</li>
						<li><a href="#" class="w3_agile_facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
						<li><a href="#" class="agile_twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
						<li><a href="#" class="w3_agile_dribble"><i class="fa fa-dribbble" aria-hidden="true"></i></a></li>
					</ul>	
				</div>
			</form>
		</div>
	</div>
	<div id="small-dialog1" class="mfp-hide w3ls_small_dialog wthree_pop">
		<h3 class="agileinfo_sign">Sign Up</h3>	
		<div class="agileits_signin_form">
			<form action="#" method="post">
				<input type="text" name="name" placeholder="First Name" required="">
				<input type="text" name="name" placeholder="Last Name" required="">
				<input type="email" name="email" placeholder="Your Email" required="">
				<input type="password" name="password" placeholder="Password" required="">
				<input type="password" name="password" placeholder="Confirm Password" required="">
				<input type="submit" value="SIGN UP">
				<p>Already a member <a href="#small-dialog" class="play-icon popup-with-zoom-anim">Sign In</a></p>
				<div class="w3agile_social_icons">
					<ul>
						<li class="wthree_follow">Follow us on :</li>
						<li><a href="#" class="w3_agile_facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
						<li><a href="#" class="agile_twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
						<li><a href="#" class="w3_agile_dribble"><i class="fa fa-dribbble" aria-hidden="true"></i></a></li>
					</ul>	
				</div>
			</form>
		</div>
	</div>
<!-- //pop-up-box -->	
<script src="../js/jquery.magnific-popup.js" type="text/javascript"></script>
<script>
	$(document).ready(function() {
	$('.popup-with-zoom-anim').magnificPopup({
		type: 'inline',
		fixedContentPos: false,
		fixedBgPos: true,
		overflowY: 'auto',
		closeBtnInside: true,
		preloader: false,
		midClick: true,
		removalDelay: 300,
		mainClass: 'my-mfp-zoom-in'
	});
																	
	});
</script>
<!-- banner -->	
	<div class="banner1">
	
	</div>
<!-- //banner -->
 