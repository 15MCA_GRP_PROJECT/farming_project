<?php
include("subhead.php");
?>

<div class="about">
		<div class="container">
			<div class="col-md-4 agileits_about_left">
				<h3 class="w3l_head">About Us</h3>
				<p class="w3ls_head_para w3ls_head_para1">Farming Assistant</p>
			</div>
			<div class="col-md-8 agileits_about_right">
				<p>Agriculture is the cultivation of land and breeding of animals and plants
						 to provide food, fiber, medicinal plants and other products to sustain and 
						 enhance life.[1] Agriculture was the key development in the rise of sedentary 
						 human civilization, whereby farming of domesticated species created food surpluses 
						 that enabled people to live in cities. The study of agriculture is known as agricultural 
						 science. The history of agriculture dates back thousands of years; people gathered wild grains 
						 at least 105,000 years ago, and began to plant them around 11,500 years ago, before they became 
						 domesticated. Pigs, sheep, and cattle were domesticated over 10,000 years ago. Crops originate from 
						 at least 11 regions of the world. Industrial agriculture based on large-scale monoculture has in the 
						 past century become the dominant agricultural method..
							<i>"AGRICULTURE is the most healthful,most useful and most noble employment of man.</i></p>
				<ul>
					<li><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Quisque posuere</li>
					<li><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Nunc erat odio</li>
					<li><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Duis dignissim</li>
					<li><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Curabitur diam</li>
				</ul>
			</div>
			<div class="clearfix"> </div>
		</div>
	</div>
	<div class="about-mid">
		<div class="container">
			<h4>FARMING IS NOT JUST A JOB, IT�S A WAY OF LIFE</h4>
		</div>
	</div>
	<div class="team">
	<div class="container">
	<div class="team-gd">
		
		
		
		
		
		
		<div class="clearfix"></div>
		</div>
	</div>
</div>
<?php
include("footer.php");
?>