<?php
include("subhead.php");
?>
<div class="w3l-gallery">
	<div class="container" style="">
		<div class="galley-grid">
			<h3 class="w3l_head w3l_head1">Gallery</h3>
			<p class="w3ls_head_para w3ls_head_para1">Farming Assistant</p>
			<span class="lft-bar-gallery"> </span>
			<span class="rit-bar-gallery"> </span>
		</div>
		<div class="w3ls-top">
			<div class="col-md-4 project">	
				<div class="projectThumbnail">
					<div class="projectThumbnailHover projcet-ro">
						<h4></h4>
						<h5></h5>
					</div>					
					<img src="images/images (1).jpg" alt="Project 01" class="img-responsive thumbnailImage">
				</div>
			</div>
			<div class="col-md-4 project">	
				<div class="projectThumbnail">
					<div class="projectThumbnailHover projcet-ro">
						<h4></h4>
						<h5></h5>
					</div>
					
					<img src="images/download.jpg" alt="Project 01" class="img-responsive thumbnailImage">
				</div>
			</div>
			<div class="col-md-4 project">	
				<div class="projectThumbnail">
					<div class="projectThumbnailHover projcet-ro">
						<h4></h4>
						<h5></h5>
					</div>
					
					<img src="images/images (3).jpg" alt="Project 01" class="img-responsive thumbnailImage">
				</div>
			</div>
			<div class="clearfix"> </div>

			<div class="col-md-4 project">	
				<div class="projectThumbnail">
					<div class="projectThumbnailHover projcet-ro">
						<h4></h4>
						<h5></h5>
					</div>
					
					<img src="images/images (4).jpg" alt="Project 01" class="img-responsive thumbnailImage">
				</div>
			</div>
			<div class="col-md-4 project">	
				<div class="projectThumbnail">
					<div class="projectThumbnailHover projcet-ro">
						<h4></h4>
						<h5></h5>
					</div>
					
					<img src="images/images.jpg" alt="Project 01" class="img-responsive thumbnailImage">
				</div>
			</div>
			<div class="col-md-4 project">	
				<div class="projectThumbnail">
					<div class="projectThumbnailHover projcet-ro">
						<h4></h4>
						<h5></h5>
					</div>
					
					<img src="images/images (2).jpg" alt="Project 01" class="img-responsive thumbnailImage">
				</div>
			</div>
			<div class="clearfix"> </div>
			<div class="col-md-4 project">	
				<div class="projectThumbnail">
					<div class="projectThumbnailHover projcet-ro">
						<h4></h4>
						<h5></h5>
					</div>
					
					<img src="images/pure-farming-1.jpg" alt="Project 01" class="img-responsive thumbnailImage">
				</div>
			</div>
			<div class="col-md-4 project">	
				<div class="projectThumbnail">
					<div class="projectThumbnailHover projcet-ro">
						<h4></h4>
						<h5></h5>
					</div>
					
					<img src="images/download (1).jpg" alt="Project 01" class="img-responsive thumbnailImage">
				</div>
			</div>
			<div class="col-md-4 project">	
				<div class="projectThumbnail">
					<div class="projectThumbnailHover projcet-ro">
						<h4></h4>
						<h5></h5>
					</div>
					
					<img src="images/download (2).jpg" alt="Project 01" class="img-responsive thumbnailImage">
				</div>
			</div>
			
			<div class="clearfix"> </div>
		</div>		
	</div>
</div>
<?php
include("footer.php");
?>