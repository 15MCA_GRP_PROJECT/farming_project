<?php
session_start();
?>
<!--
author: W3layouts
author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html lang="en">
<head>
<title>FARMING ASSISTANT</title>
<!-- for-mobile-apps -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Floriculture Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
		function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- //for-mobile-apps -->
<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
<!-- js -->
<script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
<!-- //js -->
<!-- pop-up-box -->
<link href="css/popuo-box.css" rel="stylesheet" type="text/css" media="all" />
<!-- //pop-up-box -->
<link rel="stylesheet" href="css/base.css">
<script src="js/modernizr.min.js"></script>
<script src="js/jquery.fitvids.js"></script>
<script src="js/script.js"></script>

<!-- font-awesome icons -->
<link href="css/font-awesome.css" rel="stylesheet"> 
<!-- //font-awesome icons -->
<link href="//fonts.googleapis.com/css?family=Work+Sans:100,200,300,400,500,600,700,800,900&amp;subset=latin-ext" rel="stylesheet">
<link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
<script>
function settypeuser(a)
{
	document.getElementById("type").value=a;
	$("#typediv").hide("slow");
}

	var f=0;
function settypeuser(a)
{
	document.getElementById("type").value=a;
	//alert(a);
	if(f==1)
	{
		//alert(a);
		$("#farmer").show("slow");
		$("#retailer").show("slow");
		$("#user").show("slow");
		$("#retailer").removeAttr("style");
		$("#retailer").css("width","100px","float","left","margin-left","20px");
		$("#farmer").removeAttr("style");
		$("#farmer").css("width","100px","float","left","margin-left","20px");
		$("#user").removeAttr("style");
		$("#user").css("width","100px","float","left","margin-left","20px");
		f=0;
	}
	else if(f==0)
	{
	if(a=="USER")
	{
		$("#farmer").hide("slow");
		$("#retailer").hide("slow");
		$("#user").css("background-color","green");
		f=1;
	}
	if(a=="FARMER")
	{
		$("#farmer").css("background-color","green");
		$("#retailer").hide("slow");
		$("#user").hide("slow");
		f=1;
	}
	if(a=="RETAILER")
	{
		$("#farmer").hide("slow");
		$("#retailer").css("background-color","green");
		$("#user").hide("slow");
		f=1;
	}
	}
	
	//$("#typediv").hide("slow");
	
}
</script>
</head>
	
<body>
<!-- header -->
	<div class="header">
	<div class="w3_agileits_nav">
	<div class="container">
				<div class="w3ls-nav">
					<nav class="navbar navbar-default">
							<div class="navbar-header">
								<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
									<span class="sr-only">Toggle navigation</span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
								</button>
							</div>
						<!-- Collect the nav links, forms, and other content for toggling -->
						<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
							<ul class="nav navbar-nav">
								<li><a href="index.php">Home</a></li>
								<li><a href="about.php">About</a></li>
								<li><a href="services.php" >Services</a></li>
								<li><a href="gallery.php" >Gallery</a></li>	
								<li><a href="contact.php">Contact</a></li>
							</ul>
							<div class="clearfix"> </div>							
						</div>	
					</nav>		
			</div>

			</div>
		</div>
		<div class="container">
			
			<div class="agile_header_grid">
				<div class="w3_agile_logo">
					<h1><a href="index.php">Farming Assistant</a></h1>
				</div>
				<div class="agileits_w3layouts_sign_in">
					<ul>
						
						<li><div class="agileinfo_search">
								<form action="#" method="post">
									<input type="text" name="Search" placeholder="Type text here..." required="">
									<input type="submit" value=" ">
								</form>
							</div></li>
							<li><a href="#small-dialog" class="play-icon popup-with-zoom-anim">Sign In</a></li>
					</ul>
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>
	</div>
<!-- //header -->
<!-- pop-up-box -->
	<div id="small-dialog" class="mfp-hide w3ls_small_dialog wthree_pop">
		<h3 class="agileinfo_sign">Sign In</h3>	
		<div class="agileits_signin_form">
			<form action="" method="post">
				<input type="email" name="email" placeholder="Your Email" required="">
				<input type="password" name="password" placeholder="Password" required="">
				<div class="agile_remember">
					<div class="agile_remember_left">
					</div>
					<div class="clearfix"> </div>
				</div>
				<input type="submit" value="SIGN IN" name="login">
				<p>Don't have an account <a href="#small-dialog1" class="play-icon popup-with-zoom-anim">Sign Up</a></p>
				<div class="w3agile_social_icons">
					<ul>
						<li class="wthree_follow">Follow us on :</li>
						<li><a href="#" class="w3_agile_facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
						<li><a href="#" class="agile_twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
						<li><a href="#" class="w3_agile_dribble"><i class="fa fa-dribbble" aria-hidden="true"></i></a></li>
					</ul>	
				</div>
			</form>
		</div>
	</div>
	<div id="small-dialog1" class="mfp-hide w3ls_small_dialog wthree_pop">
		<h3 class="agileinfo_sign">Sign Up</h3>	
		<div class="agileits_signin_form">
		<div id="typediv">
			<label>Choose type</label>
			<div>
				<input type="button" id="user" name="signup" value="USER" style="width:100px;float:left;margin-left:20px" onclick="settypeuser(this.value)">
				<input type="button" id="farmer" name="signup" value="FARMER" style="width:100px;float:left;margin-left:20px" onclick="settypeuser(this.value)">
				<input type="button" id="retailer" name="signup" value="RETAILER" style="width:100px;float:left;margin-left:20px" onclick="settypeuser(this.value)">
			</div>
		</div>
		<div style="height:70px"></div>
			<form action="" method="post" enctype="multipart/form-data">
				<input type="text" name="fname" placeholder="First Name" required="">
				<input type="text" name="lname" placeholder="Last Name" required="">
				<input type="email" name="email" placeholder="Your Email" required="">
				<input type="text" name="address" placeholder="Your address" required="">
				<input type="hidden" name="type" id="type" placeholder="Your address" required="">
				<input type="file" name="idproof" placeholder="Your address" required="">
				<input type="password" name="password" placeholder="Password" required="">
				<input type="password" name="cpassword" placeholder="Confirm Password" required="">
				<input type="submit" name="signup" value="SIGN UP">
				<p>Already a member <a href="#small-dialog" class="play-icon popup-with-zoom-anim">Sign In</a></p>
				<div class="w3agile_social_icons">
					<ul>
						<li class="wthree_follow">Follow us on :</li>
						<li><a href="#" class="w3_agile_facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
						<li><a href="#" class="agile_twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
						<li><a href="#" class="w3_agile_dribble"><i class="fa fa-dribbble" aria-hidden="true"></i></a></li>
					</ul>	
				</div>
			</form>
		</div>
	</div>
<!-- //pop-up-box -->	
<script src="js/jquery.magnific-popup.js" type="text/javascript"></script>
<script>
	$(document).ready(function() {
	$('.popup-with-zoom-anim').magnificPopup({
		type: 'inline',
		fixedContentPos: false,
		fixedBgPos: true,
		overflowY: 'auto',
		closeBtnInside: true,
		preloader: false,
		midClick: true,
		removalDelay: 300,
		mainClass: 'my-mfp-zoom-in'
	});
																	
	});
</script>
<!-- banner -->	
	<div class="banner1">
	
	</div>
<!-- //banner -->
	<?php
		include("connect.php");
		if(isset($_POST['signup']))
		{
			$fname=$_POST['fname'];
			$lname=$_POST['lname'];
			$email=$_POST['email'];
			$address=$_POST['address'];
			$type=$_POST['type'];
			$psw=$_POST['password'];
			$img=$_FILES['idproof']['name'];
			$check="select * from tb_login where logname='$email'";
			$che=mysql_query($check);
			$cnum=mysql_num_rows($che);
			if($cnum>0)
			{
				echo "<script>alert('Email id already exist');window.location.href='index.php';</script>";				
			}
			else
			{
				$exte=explode(".",$img);
				$ext=end($exte);
				$path="idproof/".$email.".".$ext;
				move_uploaded_file($_FILES['idproof']['tmp_name'],"$path");
				$log="INSERT INTO `tb_login` (`logname`, `logpsw`, `logtype`)VALUES('$email', '$psw', '$type')";
				$lo=mysql_query($log);
				if($lo>0)
				{
					$sql="INSERT INTO `tb_userregistration`(`fname`,`lname`,`address`,`email`,`imgpath`)VALUES('$fname','$lname','$address','$email','$path')";
					$sq=mysql_query($sql);
				}
				if($lo>0 && $sq>0)
				{
					echo "<script>alert('Registration successfully');window.location.href='index.php';</script>";
				}
				else
				{
					echo "<script>alert('Error');window.location.href='index.php';</script>";				
				}
			}
		}
		if(isset($_POST['login']))
		{
			include("connect.php");
			$email=$_POST['email'];
			$psw=$_POST['password'];
			$sql="select * from tb_login where logname='$email' and logpsw='$psw' and status='approve'";
			$sq=mysql_query($sql) or die(mysql_error());
			if($sq>0)
			{
				$s=mysql_fetch_array($sq);
				$type=$s[2];
				$uid=$s[0];
				$_SESSION['uid']=$uid;
				if($type=='USER')
				{
					echo "<script>window.location.href='USER/index.php';</script>";
				}
				elseif($type=='ADMIN')
				{
					echo "<script>window.location.href='ADMIN/index.php';</script>";
				}
				elseif($type=='FARMER')
				{
					echo "<script>window.location.href='FARMER/index.php';</script>";
				}
				elseif($type=='RETAILER')
				{
					echo "<script>window.location.href='RETAILER/index.php';</script>";
				}
				else
				{
					echo "<script>alert('Error')</script>";
				}
			}
		}

	?>

 