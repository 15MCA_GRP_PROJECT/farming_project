<?php
include("subhead.php");
?>
<div class="agileits-w3layouts-ser all_pad w3ls">
	<div class="container">
		<h3 class="w3l_head w3l_head1">Services</h3>
		<p class="w3ls_head_para w3ls_head_para1">Farming Assistant</p>
		<div class="ser-top-grids agileits">
			<div class="col-md-4 ser-grid agileinfo">
				<div class="con-left text-center">
					<div class="spa-ico"><span class="glyphicon glyphicon-cog" aria-hidden="true"></span></div>
					<h5>Farm management solutions</h5>
					<p>Information sharing, analytics and precision farming tools </p>
					
				</div>
			</div>
			<div class="col-md-4 ser-grid agileinfo">
				<div class="con-left text-center">
					<div class="spa-ico"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></div>
					<h5>Production assistance</h5>
					<p>On-site resources to aid production, such as equipment rentals.</p>
					
				</div>
			</div>
			<div class="col-md-4 ser-grid agileinfo">
				<div class="con-left text-center">
					<div class="spa-ico"><span class="glyphicon glyphicon-leaf" aria-hidden="true"></span></div>
					<h5>Access to markets</h5>
					<p>Virtual platforms that connect farmers with suppliers of seeds, fertilisers and other agrochemicals, as well as consumers of their produce.</p>
					
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>
<?php
include("footer.php");
?>