<?php
include("header.php");
?>
<div class="about">
		<div class="container">
			<div class="col-md-4 agileits_about_left">
				<h3 class="w3l_head">About Us</h3>
				<p class="w3ls_head_para w3ls_head_para1">Farming Assistant</p>
			</div>
			<div class="col-md-8 agileits_about_right">
				<p style="text-align:justify"><i>Agriculture is the backbone of the Indian Economy"</i>
				- said Mahatma Gandhi six decades ago.
				 Even today, the situation is still the same, with almost the entire economy being 
				 sustained by agriculture, which is the mainstay of the villages. It contributes 16% of the
				  overall GDP and accounts for employment of approximately 52% of the Indian population.
				   Rapid growth in agriculture is essential not only for self-reliance but also to earn 
				   valuable foreign exchange.</p>
				
			</div>
			<div class="clearfix"> </div>
		</div>
	</div>
	<!--services-->
	<div class="services">
		<div class="container">
				<h3 class="w3l_head w3l_head1">Services</h3>
			<p class="w3ls_head_para w3ls_head_para1">Farming Assistant</p>
			<div class="service-row">
				<div class="col-md-4 col-sm-4 service-gds service-gd1">
					<div class="bott-img">
						<div class="icon-holder">
							<i class="fa fa-scissors" aria-hidden="true"></i>	
						</div>
						<h4 class="mission">Farm management solutions</h4>
						<p class="description">Information sharing, analytics and precision farming tools.</p>
					</div>
				</div>
				<div class="col-md-4 col-sm-4 service-gds service-gd2">
					<div class="bott-img">
						<div class="icon-holder">
							<i class="fa fa-link" aria-hidden="true"></i>
						</div>
						<h4 class="mission">Production assistance</h4>
						<p class="description">On-site resources to aid production, such as equipment rentals.</p>
					</div>
				</div>
				<div class="col-md-4 col-sm-4 service-gds service-gd3">
					<div class="bott-img">
						<div class="icon-holder">
							<i class="fa fa-modx" aria-hidden="true"></i>
						</div>
						<h4 class="mission">Access to markets</h4>
						<p class="description">Virtual platforms that connect farmers with suppliers of seeds, fertilisers and other agrochemicals, as well as consumers of their produce.</p>
					</div>
				</div>
				<div class="col-md-4 col-sm-4 service-gds service-gd4">
					<div class="bott-img">
						<div class="icon-holder">
							<i class="fa fa-arrows" aria-hidden="true"></i>
						</div>
						<h4 class="mission">FAQ</h4>
						<p class="description">Online chat with admin to get an answer to a question.</p>
					</div>
				</div>
				<div class="col-md-4 col-sm-4 service-gds service-gd5">
					<div class="bott-img">
						<div class="icon-holder">
							<i class="fa fa-cog" aria-hidden="true"></i>
						</div>
						<h4 class="mission">Chat </h4>
						<p class="description">Online chat with farmers and personal chat is provided.</p>
					</div>
				</div>
				<div class="col-md-4 col-sm-4 service-gds service-gd6">
					<div class="bott-img">
						<div class="icon-holder">
							<i class="fa fa-random" aria-hidden="true"></i>
						</div>
						<h4 class="mission">Offers and auction</h4>
						<p class="description">Uers can purchase product and also purchase offer product.Action is another service to retailers to purchase bulk amount of product purchase through auction.</p>
					</div>
				</div>
				<div class="clearfix"> </div>				
			</div>
		</div>
	</div>
	<!--//services-->
	<!-- features -->
<div class="features">
		<div class="container">
				<h3 class="w3l_head w3l_head1">Features</h3>
			<p class="w3ls_head_para w3ls_head_para1">Farming Assistant</p>
				<div class="w3_agile_features_grids">
				<div class="col-md-3 w3_agile_features_grid">
					<div class="ih-item circle effect1 agile_features_grid">
						<div class="spinner"></div>
						<div class="img"><img src="../images/animated.gif" alt=" " class="img-responsive"></div>
					</div>
					<fieldset>
						<legend>Auction</legend>
							Retailers can purchase bulk amount of peoduct from farmers through online auction.
					</fieldset>
				</div>
				<div class="col-md-3 w3_agile_features_grid">
					<div class="ih-item circle effect1 agile_features_grid">
						<div class="spinner"></div>
						<div class="img"><img src="../images/ZAbi.gif" alt=" " class="img-responsive"></div>
					</div>
					<fieldset>
						<legend>Online chat</legend>
							farmers to farmers chat is provided.They can ask any doubt in any area of farming to a perticular farmer through this system.
					</fieldset>
				</div>
				<div class="col-md-3 w3_agile_features_grid">
					<div class="ih-item circle effect1 agile_features_grid">
						<div class="spinner"></div>
						<div class="img"><img src="../images/75_faq_animated.gif" alt=" " class="img-responsive"></div>
					</div>
					<fieldset>
						<legend>FAQ</legend>
							Online coversaction between admin and farmers.farmers can ask question to admin and admin can reply the answer through the system.
					</fieldset>
				</div>
				<div class="col-md-3 w3_agile_features_grid">
					<div class="ih-item circle effect1 agile_features_grid">
						<div class="spinner"></div>
						<div class="img"><img src="../images/wh_special-offer-green_sxu.gif" alt=" " class="img-responsive"></div>
					</div>
					<fieldset>
						<legend>Special offers</legend>
						Farmers provided offers to user and purchase special offer product through the system.
					</fieldset>
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>
	</div>
<!-- //features -->
<?php
include("footer.php");
?>